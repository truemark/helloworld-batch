FROM alpine:latest
RUN apk add --no-cache bash
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
