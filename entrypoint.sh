#!/usr/bin/env bash

set -euo pipefail
printf "Hello"
[[ -n "${1+x}" ]] && sleep "${1}"
[[ -n "${SLEEP+x}" ]] && sleep "${SLEEP}"
echo " World!"
