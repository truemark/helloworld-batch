# helloworld-batch

This is a simple Docker image used to test things like AWS batch automation. This entrypoint
for this docker image simply prints the word "Hello" sleeps the number of seconds provided
and then prints "World!"

Example with command line argument
```bash
docker run -it --rm truemark/helloworld-batch:latest 10
```

Example with environment variable
```bash
docker run -it --rm -e SLEEP=10 truemark/helloworld-batch:latest
```
